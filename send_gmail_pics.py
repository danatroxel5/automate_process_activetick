#This routine assumes that chrome is open with gmail in max screen size
#also 4 plots are open in standard size with all corners in upper left
import pyautogui
import subprocess
import time

def send_gmail_clips():
    pyautogui.click(660,1060) #click on chrome in task bar (assumes 4th icon)
    pyautogui.click(100,233) #click on compose in gmail
    pyautogui.click(1156,445) #click on To field
    pyautogui.typewrite('3605021577@tmomail.net')
    time.sleep(0.2)
    pyautogui.click(1131,537) #click into gmail body
    time.sleep(0.2)
    pyautogui.click(1254,492) #click on subject line
    pyautogui.typewrite('spy15Diff') #subject text
    time.sleep(0.2)
    pyautogui.click(1101,1059) #click on taskbar gnuplot
    pyautogui.click(832,980) #click on 1st plot
    pyautogui.click(114,46) #click on replot
    time.sleep(0.5)
    pyautogui.click(15,47) #click on copy plot to clipboard
    time.sleep(0.5)
    pyautogui.click(660,1060) #click on chrome in task bar (assumes 4th icon)
    pyautogui.click(1131,537) #click into gmail body
    pyautogui.hotkey('ctrl', 'v') #paste clipboard to gmail body

    pyautogui.click(1101,1059) #click on taskbar gnuplot
    pyautogui.click(1007,980) #click on 2nd plot
    pyautogui.click(114,46) #click on replot
    time.sleep(0.5)
    pyautogui.click(15,47) #click on copy plot to clipboard
    time.sleep(0.5)
    pyautogui.click(660,1060) #click on chrome in task bar (assumes 4th icon)
    pyautogui.click(1131,537) #click into gmail body
    pyautogui.press('enter')
    pyautogui.hotkey('ctrl', 'v') #paste clipboard to gmail body

    pyautogui.click(1101,1059) #click on taskbar gnuplot
    pyautogui.click(1185,980) #click on 3rd plot
    pyautogui.click(114,46) #click on replot
    time.sleep(0.5)
    pyautogui.click(15,47) #click on copy plot to clipboard
    time.sleep(0.5)
    pyautogui.click(660,1060) #click on chrome in task bar (assumes 4th icon)
    pyautogui.click(1131,537) #click into gmail body
    pyautogui.press('enter')
    pyautogui.hotkey('ctrl', 'v') #paste clipboard to gmail body

    pyautogui.click(1101,1059) #click on taskbar gnuplot
    pyautogui.click(1356,980) #click on 4th plot
    pyautogui.click(114,46) #click on replot
    time.sleep(0.5)
    pyautogui.click(15,47) #click on copy plot to clipboard
    time.sleep(0.5)
    pyautogui.click(660,1060) #click on chrome in task bar (assumes 4th icon)
    pyautogui.click(1131,537) #click into gmail body
    pyautogui.press('enter')
    pyautogui.hotkey('ctrl', 'v') #paste clipboard to gmail body

    time.sleep(2.0)
    pyautogui.hotkey('ctrl', 'enter') #paste clipboard to gmail body



#send_gmail_clips()