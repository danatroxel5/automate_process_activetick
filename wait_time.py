from datetime import datetime

def wait_nextmin(mytime):
    t1 = datetime.now()
    while(int(t1.minute/mytime)*mytime != t1.minute): #wait until exactly divisible
        t1 = datetime.now()

    stmin=t1.minute
    while(stmin == t1.minute): #wait until next minute elapses
        t1 = datetime.now()


def wait_min():
    t1 = datetime.now()
    t2 = t1
    while(t1.minute == t2.minute ): #wait until new minute
        t2 = datetime.now()
