#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      danat
#
# Created:     29/10/2020
# Copyright:   (c) danat 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import time
from send_text import send_text
from read_lastline import get_lastline
from pr_activetick import process_activetick
from wait_time import wait_nextmin
from wait_time import wait_min
from datetime import datetime
from send_gmail_pics import send_gmail_clips


def main_1():
    t1 = datetime.now()
    while(t1.hour<13):
        wait_nextmin(15)
        time.sleep(2) #2 seconds past

        #time.sleep((60-6.8))
        process_activetick()
        last = get_lastline("MACDVarious_Spx_15.dat","C:/tcc/NewCombo/")
        send_text(last," SP15")
        send_gmail_clips()
        t1 = datetime.now()

    print("Done Processing Excell")
    #time.sleep(3)
    #print(pyautogui.position())



def min1_process():
    last=1000
    while(last>-1.848):
        wait_min()
        time.sleep(2) #2 seconds past
        process_activetick()
#        last = get_lastline("MACDVarious_Spx_1.dat","C:/tcc/NewCombo/")
        last = get_lastline("DiffOfSums_1.dat","C:/tcc/NewCombo/")

    send_text(last," SP1<-1.848")

min1_process()

#if __name__ == '__main__':
    #send_text('yah baby')
#    main()
