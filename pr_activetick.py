import pyautogui
import subprocess
import time

def process_activetick():
    pyautogui.click(540,1060) #click on excell in task bar (assumes 1st icon)
    pyautogui.click(600,50) #click on Active Tick Add in in excell
    time.sleep(1)
    pyautogui.click(25,80) #click on GetBar
    pyautogui.typewrite('$spx')
    pyautogui.click(1026,508) #Drop down for time
    pyautogui.click(992,526) #Drop down 1Min
    pyautogui.press('enter')
    time.sleep(0.5)
    pyautogui.hotkey('ctrl', 's') #save excel file

    subprocess.call([r'C:\tcc\NewCombo\runprjs_Spxt.bat'])
    #subprocess.call([r'C:\tcc\NewCombo\runprjs_Spxt_LongShort.bat'])
    pyautogui.click(540,1060) #click again excell in task bar (assumes 1st icon)
    #pyautogui.click(561,1068) #open python window
    pyautogui.click(580,1060) #minimize python window
    time.sleep(0.5)

    #pyautogui.click(500,1060) #minimize excell in task bar (assumes 1st icon)