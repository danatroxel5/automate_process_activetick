#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      danat
#
# Created:     13/04/2021
# Copyright:   (c) danat 2021
# Licence:     <your licence>
#-------------------------------------------------------------------------------

def main():
    birth_year = input("What is your Birth Year?")
    age = 2021-int(birth_year)
    print('At the end of this year your age is = ' + str(age))

if __name__ == '__main__':
    main()
